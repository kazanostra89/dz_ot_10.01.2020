﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z2
{
    class Program
    {
        static void Main(string[] args)
        {
            double result;

            result = (2.0 * Factorial(5) + 3.0 * Factorial(8)) / (Factorial(6) + Factorial(4));

            Console.WriteLine("Значение выражения = " + result);

            Console.ReadKey();
        }
        
        static double Factorial(int inputNumberFactorial)
        {
            double factorial = 1.0;

            for (double i = 2.0; i <= inputNumberFactorial; i++)
            {
                factorial = factorial * i;
            }

            return factorial;
        }
    }
}
