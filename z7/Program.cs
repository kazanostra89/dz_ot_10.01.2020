﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z7
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;

            a = Vvod("Input a: ");
            b = Vvod("Input b: ");

            DraingLines(a, b);

            Console.ReadKey();
        }

        static int Vvod(string message)
        {
            int inputNumber;
            bool check;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out inputNumber);

            } while (check == false);

            return inputNumber;
        }

        static void DraingLines(int a, int b)
        {
            for (int i = 0; i < a; i++)
            {
                Drawing(b);
                Console.WriteLine();
            }
        }

        static void Drawing(int numberStar)
        {
            for (int i = 0; i < numberStar; i++)
            {
                Console.Write("*");
            }
        }
    }
}

