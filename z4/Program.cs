﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z4
{
    class Program
    {
        static void Main(string[] args)
        {
            int A, B;

            A = Vvod("Input number A: ");
            B = Vvod("Input number B: ");

            Console.WriteLine("\nЧетные числа в диапазоне от " + A + " до " + B + ":\n");

            Perebor(A, B);

            Console.ReadKey();
        }
        

        static int Vvod(string message)
        {
            int inputNumber;
            bool chek;

            do
            {
                Console.Write(message);
                chek = int.TryParse(Console.ReadLine(), out inputNumber);

            } while (chek == false);


            return inputNumber;
        }

        static void Perebor(int A, int B)
        {
            for (int i = A; i <= B; i++)
            {
                switch (CheckNumber(i))
                {
                    case true:
                        Console.Write(i + " ");
                        break;
                    case false:
                        break;
                }
            }
        }
        
        static bool CheckNumber(int checkInputNumber)
        {
            if (checkInputNumber % 2 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
