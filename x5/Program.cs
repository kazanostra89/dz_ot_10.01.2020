﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace x5
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;

            a = InputNumber("Input a: ");
            b = InputNumber("Input b: ");

            Console.WriteLine("\nПериметр прямоугольника = " + Perimetr(a, b));

            Console.ReadKey();
        }


        static int InputNumber(string message)
        {
            int number;
            bool check;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false);

            return number;
        }

        static int Perimetr(int a, int b)
        {
            return (a + b) * 2;
        }

    }
}
