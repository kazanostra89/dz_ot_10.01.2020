﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z6
{
    class Program
    {
        static void Main(string[] args)
        {
            int inputNumber;
            bool check;

            do
            {
                Console.Write("Введите число: ");
                check = int.TryParse(Console.ReadLine(), out inputNumber);

            } while (check == false);

            Console.WriteLine();
            Drawing(inputNumber);

            Console.ReadKey();
        }

        static void Drawing(int numberStar)
        {
            for (int i = 0; i < numberStar; i++)
            {
                Console.Write("*");
            }
        }

    }
}
