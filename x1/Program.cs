﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace x1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, max;

            a = InputNumber("Input a: ");
            b = InputNumber("Input b: ");

            switch (SearchMax(a, b))
            {
                case 0:
                    Console.WriteLine("\nEquality");
                    break;
                default:
                    Console.WriteLine("\nMax number: " + SearchMax(a, b));
                    break;
            }

            Console.ReadKey();
        }


        static int InputNumber(string message)
        {
            int number;
            bool check;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false);

            return number;
        }

        static int SearchMax(int numberOne, int numberTwo)
        {
            int max = 0;
            
            if (numberOne > numberTwo)
            {
                max = numberOne;
            }
            else if (numberOne < numberTwo)
            {
                max = numberTwo;
            }

            return max;
        }

    }
}
