﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace x4
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;

            a = InputNumber("Input a: ");
            b = InputNumber("Input b: ");

            Console.WriteLine("\nПлощадь прямоугольника = " + Square(a, b));

            Console.ReadKey();
        }


        static int InputNumber(string message)
        {
            int number;
            bool check;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false);

            return number;
        }

        static int Square(int a, int b)
        {
            return a * b;
        }
    }
}
