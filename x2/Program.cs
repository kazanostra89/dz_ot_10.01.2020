﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace x2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 6;
            int[] mas = new int[n];
            bool check;

            for (int i = 0; i < n; i++)
            {

                do
                {
                    Console.Write("Введите число " + (i + 1) + " = ");
                    check = int.TryParse(Console.ReadLine(), out mas[i]);

                } while (check == false);

            }

            Console.WriteLine("\n" + MaxNumberMas(mas));

            Console.ReadKey();
        }


        static int MaxNumberMas(int[] masMax)
        {
            int max = 0;

            for (int i = 0; i < masMax.Length; i++)
            {
                if (masMax[i] > max)
                {
                    max = masMax[i];
                }
            }
            
            return max;
        }
        
    }
}
