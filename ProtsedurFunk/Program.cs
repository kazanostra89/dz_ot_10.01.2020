﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtsedurFunk
{
    class Program
    {

        static int InputDano(string msg)
        {
            bool inputResult;
            int dano;

            do
            {
                Console.Write(msg);
                inputResult = int.TryParse(Console.ReadLine(), out dano);
            } while (inputResult == false);

            return dano;
        }

        static int Discr(int a, int b, int c)
        {

            return b * b - 4 * a * c;

        }


        static void Main(string[] args)
        {
            int a, b, c, discrim;

            a = InputDano("Введите а: ");
            b = InputDano("Введите b: ");
            c = InputDano("Введите c: ");

            discrim = Discr(a, b, c);


            Console.WriteLine(discrim);

            Console.ReadKey();
        }
    }
}
