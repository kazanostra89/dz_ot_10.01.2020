﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z3
{
    class Program
    {
        static void Main(string[] args)
        {
            bool check;
            int inputNumber;

            do
            {
                Console.Write("Введите число для проверки его четности: ");
                check = int.TryParse(Console.ReadLine(), out inputNumber);

            } while (check == false);

            switch (CheckNumber(inputNumber))
            {
                case true:
                    Console.WriteLine("Введенное число " + inputNumber + " четное!");
                    break;
                case false:
                    Console.WriteLine("Введенное число " + inputNumber + " нечетное!");
                    break;
            }

            Console.ReadKey();
        }

        static bool CheckNumber(int checkInputNumber)
        {
            if (checkInputNumber % 2 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
                
    }
}
