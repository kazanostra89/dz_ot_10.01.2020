﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c;

            a = Vvod("Input a: ");
            b = Vvod("Input b: ");
            c = Vvod("Input c: ");

            Console.WriteLine("Дискриминант равен: " + Disc(a, b, c));

            Console.ReadKey();
        }
        
        static int Disc(int a, int b, int c)
        {
            return (int)Math.Pow(b, 2.0) - 4 * a * c;
        }

        static int Vvod(string message)
        {
            int input;
            bool check;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out input);

            } while (check == false);
            
            return input;
        }

    }
}
